const fs = require("fs")
const fsp = require("fs").promises
const path = require("path")

function copyFileSync(file) {
  try {
    log("start")
    const dir = fs.mkdtempSync("test-")
    const buffer = fs.readFileSync(file)
    fs.writeFileSync(path.join(dir, file), buffer)
    log(`${file} written to ${dir}`)
  } catch (r) {
    log("catch " + r)
  }
}

function copyFileAsync(file) {
  try {
    log("start")
    fs.mkdtemp("test-", (r, dir) => {
      if (r) log("mkd " + r)
      else fs.readFile(file, (r, buffer) => {
        if (r) log("read " + r)
        else fs.writeFile(path.join(dir, file), buffer, (r) => {
          if (r) log("write " + r)
          else log(`${file} written to ${dir}`)
        })
      })
    })
  } catch (r) {
    log("catch " + r)
  }
}

function copyFilePromiseDeeltje(file) {
  log("start")
  fs.mkdtemp("test-", (r, dir) => {
    if (r) log("mkd " + r)
    else fsp.readFile(file)
      .then(buffer => fsp.writeFile(path.join(dir, file), buffer))
      .then(() => {
        log(`${file} written to ${dir}`)
      })
      .catch(r => {
        if (r) log("write " + r)
      })
  })
}

function copyFilePromise(file) {
  log("start")
  let dir;
  fsp.mkdtemp("test-")
    .then(tmpDir => {
      dir = tmpDir
      return fsp.readFile(file)
    })
    .then(buffer => fsp.writeFile(path.join(dir, file), buffer))
    .then(() => {
      log(`${file} written to ${dir}`)
    })
    .catch(r => {
      if (r) log("write " + r)
    })
}

async function copyFileAwait(file) {
  try {
    log("start")
    let dir = await fsp.mkdtemp("test-")
    let buffer = await fsp.readFile(file)
    await fsp.writeFile(path.join(dir, file), buffer);
    log(`${file} written to ${dir}`)
  } catch (r) {
    if (r) log("write " + r)
  }
}


function log(txt) {
  console.log(`${timestamp()}  ${txt}`)
}

function timestamp() {
  const now = new Date()
  return `${now.getMinutes()}:${now.getSeconds()}:${now.getMilliseconds()}`
}

//copyFileSync("sample.txt")
//copyFileAsync("sample.txt")
//copyFilePromise("sample.txt")
copyFileAwait("sample.txt")
log("the end")